# gpx2cot

GPX player to Cursor-on-target (CoT)

Reads one or more GPS tracks in timestamped GPX format and replays them, at original speed, to a CoT network.

## applications
- traffic generation for software development and testing
- live action simulation for user training and assessment
- mission log visualizer for after-action review

Any object type supported by ATAK can be simulated (with a suitable GPX file): ground units, vehicles, aircraft and more.

## installation
The program is self-contained and can run from any location. Requires Python version 2 or 3 with standard libraries. No external packages required.

## operation
Run the program giving one or more GPX files as command-line parameters:

`gpx2cot.py file1.gpx file2.gpx ...`

GPX tracks will be played back in parallel, at the original speed, using an animated CoT point for each track, which may be occasionally referred to as an "actor".
Playback can be performed once (program exits when last track ends) or in an infinite loop (program runs forever or until interrupted).

By default, CoT messages will be sent to multicast address 239.2.3.1 port 6969 (default ATAK P2P network).

## configuration
gpx2cot behaviour is controlled by environment variables

| env var | description | default |
|---|---|---|
|ATAK_HOST|CoT destination address|239.2.3.1|
|ATAK_PORT|CoT destination port|6969|
|UNIT_TYPE|actor TAK display type|a-f-G-U-C (friendly-ground-unit-combat)|
|UNIT_TEAM|actor TAK team|Maroon|
|UNIT_ROLE|actor TAK role|"Team Member"|
|BASE_NAME|prefix for actor names|ZOMBIE|
|LOOP|replay the tracks in a loop|0|
|DEBUG|enable debugging output|0|

## compatibility
### GPX formats
In general, all timestamped GPX files should be well supported. Tracks without timestamps are unsupported for obvious reasons.

In particular, files exported using ATAK's "Tracks / Export / GPX" function should work well.

If the GPX file contains the `ele` attribute (elevation), it will be used, however it is not mandatory.
Speed, azimuth and GPS accuracy attributes are currently unsupported.

---
Copyright (c) 2020 by Alec Murphy - MIT licensed
